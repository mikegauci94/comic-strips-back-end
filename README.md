# Tiny Test - Comic Strips App

Production environment - https://comic-strips-app-tiny.herokuapp.com/api

## Technologies used

- NodeJs and Express (For proxy server)
- XKCD API (For fetching comics)

This project is being used as a proxy server to bypass CORS in conjunction with the Comic Strips Front-End project