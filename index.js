const app = require("express")();
const request = require("request");
const cors = require("cors");

const port = process.env.PORT || 3500;

app.get("/", (req, res) => {
  res.send("Hello please navigate to /api to access the api");
});

app.get("/api", (req, res) => {
  const number = req.query["num"];
  let url = `https://xkcd.com/info.0.json`;

  if (number) {
    url = `https://xkcd.com/${number}/info.0.json`;
  }

  res.setHeader(
    "Access-Control-Allow-Origin",
    req.header("origin") ||
      req.header("x-forwarded-host") ||
      req.header("referer") ||
      req.header("host")
  );

  res.setHeader("Access-Control-Allow-Methods", "POST, OPTIONS");

  request(url).pipe(res);
});

app.use(cors());

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
